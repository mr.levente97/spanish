# Language Transfer

## Lesson 2

Words ending in -al tend to come from Latin
> normal, metal

es -> is, it is, she is, he is, you are (formal)
> es normal -> he.she is normal
> es legal -> it's legal
> 
> es ilegal -> it's illegal

if it's known who you are talking about you don't need he/she, you can just use "es"
> es liberal -> he/she is liberal

no -> not, don't, no

put it before "es" to make it it's not
> no es ideal -> it's not ideal

more examples for -al ending:
> fatal, natal, colonial, cultural
>
> anual -> annual
>
> festival, personal, total, verbal

Where we don't have a rule we can try to stick an -o, -i, -e, -a on the end guessing it

> verb -> el verbo

## Lesson 3

ending -ant, -ent, stick an -e to the end

> important -> importante
>
> diferente, constante

add -mente on the end to get -ly in english

> constant -> constante
> 
> constantly -> constantemente
> 
> legalmente
> 
> real & royal -> real -> realmente
> 
> normally / usually -> normalmente
> 
> (usually) normally it's important -> normalmente es importante

"dzs" sound (type of G) becomes H sound, but not all G
> Argentina -> pronounce \[Arhentina\]
> 
> Global -> G doesn't change
> Globally -> Globalmente
> 
> General -> pronounce \[Heneral\]
> 
> Original \[Orihinal\] -> Originalmente \[Orihinalmente\]
>
> Naturally/Obviously it's original -> Naturalmente es original

words ending -ible, -able just pronounce them syllable by syllable
> possible -> posible
> probable -> probable
> flexible -> flexible

## Lesson 4

## Lesson 5
