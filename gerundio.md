## English -ing ending -> estar + **ando** & **iendo**

What you are doing while talking, basically no beginning or ending point

¿Qué haces? -> What do you do? (To ask what are you doing)

¿Qué estás haciendo? -> What are you doing?

> yo estoy
>
> tu estás
>
> él,ella,ud. está
>
> nosotros estamos
>
> vosotros estáis
>
> ellos,ellas,ustedes están

ar -> ando

er & ir -> iendo

camin~~ar~~ -> caminando

com~~er~~ -> comiendo

sal~~ir~~ -> saliendo

double vowel words:

leer would be _leiendo_ but becomes **leyendo**

yo cocino (todos los dias) is not the same as estoy cocinando (I'm cooking, right now, while talking) because spanish can't express progressive with the present

to be read: https://spanish.kwiziq.com/revision/glossary/verb-structure/la-perifrasis-verbal
