|                     | masculine | feminine |
| ------------------- | --------- | -------- |
| definite singular   | el        | la       |
| definite plural     | los       | las      |
| indefinite singular | un        | una      |
| indefinite plural   | unos      | unas     |

abstract nouns derived from adjectives use neutral "lo" and has no plural form

> lo bueno - the good
>
> lo malo - the bad
