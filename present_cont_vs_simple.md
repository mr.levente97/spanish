# present continuous / progressive < == > present simple

## present continuous / progressive

Estar (shows who's doing the action) + [gerundio](#gerundio)

> estoy/estás/está/estamos/estáis/están

me, te, lo, la, le, se, nos os, los, las, les

either before estar separately or connected to the main verb

> Me estoy duchando / Estoy duchándome -> I'm taking a shower

### Usage:

Ongoing actions

> ¿Qué estáis haciendo? - What are you (pl) doing right now?
>
> Estamos cenando - Having dinner

Nowadays, since some time, and still ongoing

> Últimamente está trabajando mucho - He's working a lot lately
>
> Aquí está lloviendo desde hace días - It's been raining for days here

Repeated actions to which the speaker is reacting emotionally (eg: annoyance or happiness)

> Mamá, Pablo siempre está molestándome - Mom Pablo is bothering me all the time
>
> Siempre estoy pensando en ti - I'm always thinking of you

### gerundio

<b>ar -> ando

er & ir -> iendo</b>

camin~~ar~~ -> caminando

com~~er~~ -> comiendo

sal~~ir~~ -> saliendo

<b>Words ending with vowels -> -yendo:</b>

> oir - oyendo
>
> creer - creyendo
>
> construir - construyendo
>
> leer - leyendo
>
> ir - yendo
>
> caer - cayendo

<b>Irregular</b>

Verbs that are irregular in present simple:

<b>| e -> i | e -> ie | o -> ue |</b>

decir -> diciendo <br>
pedir -> pidiendo <br>
dormir -> durmiendo <br>
sentir -> sintiendo <br>
vestir -> vistiendo <br>
servir -> sirviendo <br>
divertir -> divirtiendo <br>
morir -> muriendo <br>
venir -> viniendo <br>
repetir -> repitiendo

## present simple
