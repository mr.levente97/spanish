# Regular

# By Ending

## -AR

| present indicative tense |     Singular      |        Pluar        |
| :----------------------- | :---------------: | :-----------------: |
| 1st Person               | \[STEM\] + **o**  | \[STEM\] + **amos** |
| 2nd Person               | \[STEM\] + **as** | \[STEM\] + **áis**  |
| 3rd Person               | \[STEM\] + **a**  |  \[STEM\] + **an**  |

| preterite tense (past) |      Singular       |         Pluar         |
| :--------------------- | :-----------------: | :-------------------: |
| 1st Person             |  \[STEM\] + **é**   |  \[STEM\] + **amos**  |
| 2nd Person             | \[STEM\] + **aste** | \[STEM\] + **asteis** |
| 3rd Person             |  \[STEM\] + **ó**   |  \[STEM\] + **aron**  |

| future indicative tense |        Singular         |           Pluar           |
| :---------------------- | :---------------------: | :-----------------------: |
| 1st Person              | \[INFINITIVE\] + **é**  | \[INFINITIVE\] + **emos** |
| 2nd Person              | \[INFINITIVE\] + **ás** | \[INFINITIVE\] + **éis**  |
| 3rd Person              | \[INFINITIVE\] + **á**  |  \[INFINITIVE\] + **án**  |

## -ER

| present indicative tense |     Singular      |        Pluar        |
| :----------------------- | :---------------: | :-----------------: |
| 1st Person               | \[STEM\] + **o**  | \[STEM\] + **emos** |
| 2nd Person               | \[STEM\] + **es** | \[STEM\] + **éis**  |
| 3rd Person               | \[STEM\] + **e**  |  \[STEM\] + **en**  |

| preterite tense (past) |      Singular       |         Pluar         |
| :--------------------- | :-----------------: | :-------------------: |
| 1st Person             |  \[STEM\] + **í**   |  \[STEM\] + **imos**  |
| 2nd Person             | \[STEM\] + **iste** | \[STEM\] + **isteis** |
| 3rd Person             |  \[STEM\] + **ió**  | \[STEM\] + **ieron**  |

| future indicative tense |        Singular         |           Pluar           |
| :---------------------- | :---------------------: | :-----------------------: |
| 1st Person              | \[INFINITIVE\] + **é**  | \[INFINITIVE\] + **emos** |
| 2nd Person              | \[INFINITIVE\] + **ás** | \[INFINITIVE\] + **éis**  |
| 3rd Person              | \[INFINITIVE\] + **á**  |  \[INFINITIVE\] + **án**  |

## -IR

| present indicative tense |     Singular      |        Pluar        |
| :----------------------- | :---------------: | :-----------------: |
| 1st Person               | \[STEM\] + **o**  | \[STEM\] + **imos** |
| 2nd Person               | \[STEM\] + **es** |  \[STEM\] + **ís**  |
| 3rd Person               | \[STEM\] + **e**  |  \[STEM\] + **en**  |

| preterite tense (past) |      Singular       |         Pluar         |
| :--------------------- | :-----------------: | :-------------------: |
| 1st Person             |  \[STEM\] + **í**   |  \[STEM\] + **imos**  |
| 2nd Person             | \[STEM\] + **iste** | \[STEM\] + **isteis** |
| 3rd Person             |  \[STEM\] + **ió**  | \[STEM\] + **ieron**  |

| future indicative tense |        Singular         |           Pluar           |
| :---------------------- | :---------------------: | :-----------------------: |
| 1st Person              | \[INFINITIVE\] + **é**  | \[INFINITIVE\] + **emos** |
| 2nd Person              | \[INFINITIVE\] + **ás** | \[INFINITIVE\] + **éis**  |
| 3rd Person              | \[INFINITIVE\] + **á**  |  \[INFINITIVE\] + **án**  |

# By Tense

## [present indicative tense](./Tenses.md#presente-simple-present-simple)

| -AR        |     Singular      |        Pluar        |
| :--------- | :---------------: | :-----------------: |
| 1st Person | \[STEM\] + **o**  | \[STEM\] + **amos** |
| 2nd Person | \[STEM\] + **as** | \[STEM\] + **áis**  |
| 3rd Person | \[STEM\] + **a**  |  \[STEM\] + **an**  |

| -ER        |     Singular      |        Pluar        |
| :--------- | :---------------: | :-----------------: |
| 1st Person | \[STEM\] + **o**  | \[STEM\] + **emos** |
| 2nd Person | \[STEM\] + **es** | \[STEM\] + **éis**  |
| 3rd Person | \[STEM\] + **e**  |  \[STEM\] + **en**  |

| -IR        |     Singular      |        Pluar        |
| :--------- | :---------------: | :-----------------: |
| 1st Person | \[STEM\] + **o**  | \[STEM\] + **imos** |
| 2nd Person | \[STEM\] + **es** |  \[STEM\] + **ís**  |
| 3rd Person | \[STEM\] + **e**  |  \[STEM\] + **en**  |

## [preterite tense (past)](./Tenses.md#pretérito-indefinido-preterite)

| -AR        |      Singular       |         Pluar         |
| :--------- | :-----------------: | :-------------------: |
| 1st Person |  \[STEM\] + **é**   |  \[STEM\] + **amos**  |
| 2nd Person | \[STEM\] + **aste** | \[STEM\] + **asteis** |
| 3rd Person |  \[STEM\] + **ó**   |  \[STEM\] + **aron**  |

| -ER & -IR  |      Singular       |         Pluar         |
| :--------- | :-----------------: | :-------------------: |
| 1st Person |  \[STEM\] + **í**   |  \[STEM\] + **imos**  |
| 2nd Person | \[STEM\] + **iste** | \[STEM\] + **isteis** |
| 3rd Person |  \[STEM\] + **ió**  | \[STEM\] + **ieron**  |

## [future indicative tense](./Tenses.md#futuro-simple-future-simple)

| -AR & -ER & -IR |        Singular         |           Pluar           |
| :-------------- | :---------------------: | :-----------------------: |
| 1st Person      | \[INFINITIVE\] + **é**  | \[INFINITIVE\] + **emos** |
| 2nd Person      | \[INFINITIVE\] + **ás** | \[INFINITIVE\] + **éis**  |
| 3rd Person      | \[INFINITIVE\] + **á**  |  \[INFINITIVE\] + **án**  |
