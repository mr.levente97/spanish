# General

Notes and a quick guide during my Spanish studies

[articles](./articles.md)

[demonstratives](./demonstratives.md)

## Tenses

[present continuous < == > present simple](./present_cont_vs_simple.md)

[Some basic conjugation (present, preterite, future) ](./starting_tense_conjugation.md)
