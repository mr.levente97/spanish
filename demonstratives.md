closest ===> farthest

| neutral   | essto | pl    | eso | pl   | aquello | pl       |
| --------- | ----- | ----- | --- | ---- | ------- | -------- |
| masculine | este  | estos | ese | esos | aquel   | aquellos |
| feminine  | esta  | estas | esa | esas | aquella | aquellas |

these will match the nouns in gender and number:

> este libro (this book)
>
> esa pluma (that pen)
>
> aquellos libros (those books over there)

Questions (no plural form) :

¿Qué es esto? - What is this / What are these? (Close to the speaker or in their hand)

¿Qué es eso? - What is that / what are those?(Not too far)

¿Qué es aquello? - What is that / what are those over there? (Far from the speakers)
